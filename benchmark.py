#!/usr/bin/env python

from subprocess import check_output, STDOUT, Popen
from sys import exit
from time import sleep
from os import setsid, killpg
from signal import SIGTERM
import re


def main():
    check_depencies()
    print 'MIDDLEWARE,Request per second'
    print 'none,' + benchmark('test_project.settings_no_middleware')
    print 'common,' + benchmark('test_project.settings_common_middleware')
    print 'CSRF,' + benchmark('test_project.settings_csrf_middleware')
    print 'session,' + benchmark('test_project.settings_session_middleware')
    print 'authentication + session,' + \
        benchmark('test_project.settings_session_authentication_middleware')
    print 'message + session:,' + \
        benchmark('test_project.settings_session_message_middleware')


def benchmark(settings_module, port=7148):
    cmd = 'python test_project/manage.py runserver --noreload --nostatic ' + \
          '--settings=%s %d' % (settings_module, port)
    django_process = Popen(cmd, stdout=open('/dev/null', 'w'), stderr=STDOUT,
                           preexec_fn=setsid, shell=True)
    sleep(5)  # Wait to django startup
    siege_output = shell('siege -b -t 1M http://localhost:%d/' % port)
    killpg(django_process.pid, SIGTERM)
    return get_transaction_rate(siege_output)


def get_transaction_rate(siege_output):
    m = re.search(r'^Transaction rate:\s*(\S+)', siege_output, re.M)
    if m is None:
        return None
    return m.group(1)


def check_depencies():
    check_django()
    check_siege()


def check_django(version='1.5'):
    try:
        from django import get_version
    except ImportError:
        exit('ERROR: Django is not installed')
    if get_version().startswith(version):
        return
    exit('ERROR: Django version is not ' + version)


def check_siege():
    try:
        shell('siege')
    except Exception:
        exit('ERROR: Can not execute "siege". Is it installed?')


def shell(command):
    return check_output(command, stderr=STDOUT, shell=True)

if __name__ == '__main__':
    main()
